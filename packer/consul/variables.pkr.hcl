#> common
variable "project_root" {
  type = string
}

variable "playbook_tags" {
  type = string
  default = "all"
}
variable "playbook_skip_tags" {
  type = string
  default = ""
}

variable "purge_certificate_temporary_directory" {
  type = string
  default = "TRUE"
}

variable "one_step_certificate_ca_url" {
  type      = string
}

variable "one_password_service_account_read_token" {
  type      = string
  sensitive = true
}
variable "one_password_service_account_write_token" {
  type      = string
  sensitive = true
}
variable "one_password_vault_id" {
  type      = string
  sensitive = true
}
variable "one_password_inventory_item_id" {
  type      = string
  sensitive = true
}
variable "one_password_hashistack_ca_id" {
  type      = string
  sensitive = true
}
variable "one_password_consul_server_crt_id" {
  type      = string
  sensitive = true
}
variable "one_password_consul_server_key_id" {
  type      = string
  sensitive = true
}
variable "one_password_consul_connect_ca_id" {
  type      = string
  sensitive = true
}

variable "tailscale_authkey" {
  type      = string
  sensitive = true
}
variable "tailscale_up_skip" {
  type = bool
}
variable "tailscale_state" {
  type = string
}
variable "tailscale_args" {
  type = string
}

variable "consul_debug" {
  type = bool
}
variable "consul_log_level" {
  type = string
}
variable "consul_network_interface" {
  type = string
}
variable "consul_force_install" {
  type = bool
}
variable "consul_server_join" {
  type = set(string)
}
variable "consul_env_vars" {
  type = set(string)
}
variable "consul_server_env_vars" {
  type = set(string)
}
variable "consul_client_env_vars" {
  type = set(string)
}
#<