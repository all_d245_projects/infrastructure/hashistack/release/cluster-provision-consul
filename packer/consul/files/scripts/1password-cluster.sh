#!/usr/bin/env bash

set -o errtrace -o nounset -o pipefail -o errexit

render_inventory_file() {
  ansible_inventory_file_dir="${PROJECT_ROOT}/output/consul"
  ansible_inventory_file_path=$ansible_inventory_file_dir/inventory

  mkdir -p $ansible_inventory_file_dir

  response=$(op item get "$OP_INVENTORY_DOCUMENT_ID" --vault $OP_HASHISTACK_VAULT_ID --format=json)
  inventory=$(echo "$response" | jq -r '.fields[] | select(.id == "notesPlain") | .value')
  printf "%s" "$inventory" > $ansible_inventory_file_path

  # inject secret refrences
  op inject --force --in-file $ansible_inventory_file_path --out-file $ansible_inventory_file_path
}

## MAIN ##
render_inventory_file
