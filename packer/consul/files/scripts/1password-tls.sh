#!/usr/bin/env bash

set -o errtrace -o nounset -o pipefail

#vars
purge_certs=$PURGE_CERTS

consul_certificate_tmp_directory=$PROJECT_ROOT/output/consul/tls
consul_server_crt_file_path=$consul_certificate_tmp_directory/crt
consul_server_key_file_path=$consul_certificate_tmp_directory/key
consul_server_ca_file_path=$consul_certificate_tmp_directory/ca

one_step_certificate_ca_url=$ONE_STEP_CERTIFICATE_CA_URL

one_password_vault_id=$OP_HASHISTACK_VAULT_ID
one_password_consul_crt_id=$ONE_PASSWORD_CONSUL_CRT_ID
one_password_consul_key_id=$ONE_PASSWORD_CONSUL_KEY_ID
one_password_hashistack_ca_id=$ONE_PASSWORD_HASHISTACK_CA_ID

certificate_expires_in_check="24h"

init() {
  mkdir -p $consul_certificate_tmp_directory
}

fetch_certs_from_1p() {
  echo "fetching consul_server.ca from 1password..."
  op document get \
  $one_password_hashistack_ca_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$consul_server_ca_file_path"

  echo "fetching consul_server.crt from 1password..."
  op document get \
  $one_password_consul_crt_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$consul_server_crt_file_path"

  echo "fetching consul_server.key from 1password..."
  op document get \
  $one_password_consul_key_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$consul_server_key_file_path"

}

cert_needs_renewal() {
  step certificate needs-renewal \
  $consul_server_crt_file_path \
  --expires-in $certificate_expires_in_check
}

renew_certs() {
  step ca renew \
  $consul_server_crt_file_path \
  $consul_server_key_file_path \
  --force \
  --root $consul_server_ca_file_path \
  --ca-url $one_step_certificate_ca_url
}

push_certs_to_1p() {
  echo "pushing vault_server.crt to 1password..."
  op document edit \
  $one_password_consul_crt_id \
  $consul_server_crt_file_path \
  --vault $one_password_vault_id
}

cleanup() {
  if [[ -n "$purge_certs" ]] && [[ "${purge_certs}" == "TRUE" ]]; then
    echo "deleting certificate directory at $consul_certificate_tmp_directory..."
    rm -fr $consul_certificate_tmp_directory
  else
    echo "WARNING: CERTIFICATES HAVE NOT BEEN PURGED."
  fi
}

## MAIN ##
init

fetch_certs_from_1p

cert_needs_renewal

if [ $? -eq 0 ]; then
    echo "Renewing certificate..."

    renew_certs    

    push_certs_to_1p
fi

cleanup