# HashiStack: Consul

The Consul builder configures a [consul](https://www.consul.io) cluster by installing and configuring the consul agent in either server or client modes. Provisioning is handled by shell scripts via the [shell-local](https://developer.hashicorp.com/packer/docs/provisioners/shell-local) provisioner as well as the [ansible provisioner](https://developer.hashicorp.com/packer/integrations/hashicorp/ansible).

**<u>Configurations</u>** 
Base configurations are managed by the `ansible/playbooks/consul/tasks/facts.yml` file. Additionally some configurations are managed by the `packer/consul/variables.pkrvars.hcl` file.

In addition, some configurations are stored in the [1password](https://1password.com) vault `Hashistack`. The configuration are pulled from 1password using a 1password [service account](https://developer.1password.com/docs/service-accounts/) and the 1password [cli utility](https://developer.1password.com/docs/service-accounts/use-with-1password-cli).

**<u>Running the provisioner</u>**
All commands should be executed fromt he root directory.

1. Install all required dependencies locally:
   - [docker](https://docs.docker.com/get-docker/)
   - [packer](https://developer.hashicorp.com/packer/install?product_intent=packer)

2. Ensure the [docker-consul]([../docker/README.md](https://gitlab.com/all_d245_projects/infrastructure/hashistack/docker/-/blob/dev/packer/docker-consul/README.md)) image has been built locally so the image is available locally.

3. Install the requried packer plugins.
   ```bash
   docker run \
   --rm \
   -v ${PWD}:/project_root \
   hashistack/docker-consul:local packer init -only=consul-cluster.null.null /project_root/packer/consul
   ```

4. Validate the configuration
   ```bash
   docker run \
   --rm \
   -v ${PWD}:/project_root \
   hashistack/docker-consul:local packer validate -only=consul-cluster.null.null /project_root/packer/consul
   ```

5. Run the builder to provision the consul nodes.
   ```bash
   docker run \
   --rm \
   --privileged \
   --device /dev/net/tun:/dev/net/tun \
   -e TAILSCALE_AUTHKEY={docker-exec-tailscale-auth-key} \
   -v ${PWD}:/project_root \
   hashistack/docker-consul:local packer build -only=consul-cluster.null.null /project_root/packer/consul
   ```

**Notes:**
- There are various configurations pulled from 1password. Ensure they exist and have been configured running the build.
- If its required to view additional logs, you can pass the `PACKER_LOG` environment variable to the `docker run` command: `-e PACKER_LOG=1`.